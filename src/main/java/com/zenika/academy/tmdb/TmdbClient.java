package com.zenika.academy.tmdb;

import info.movito.themoviedbapi.TmdbApi;
import info.movito.themoviedbapi.model.MovieDb;
import info.movito.themoviedbapi.model.core.MovieResultsPage;
import info.movito.themoviedbapi.model.people.PersonCast;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.time.Year;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class TmdbClient {

    private final TmdbApi client;

    /**
     * Builds a client that uses the given api key.
     * <p>
     * The api key must be in a text file with no other content.
     *
     * @param apiKeyFile the file where the api key lies.
     * @throws IOException if the file can't be read or can't be found.
     */
    public TmdbClient(File apiKeyFile) throws IOException {
        String apiKey = new String(new FileInputStream(apiKeyFile).readAllBytes());
        this.client = new TmdbApi(apiKey);
    }

    /**
     * Get info about the movie given in the parameters
     */
    public Optional<MovieInfo> getMovie(String title) {
        final MovieResultsPage movieDbs = this.client.getSearch().searchMovie(title, null, "", false, 1);

        try {
            final MovieDb rawMovie = movieDbs.getResults().get(0);
            final List<PersonCast> rawCast = this.client.getMovies().getCredits(rawMovie.getId()).getCast();
            final Year releaseYear = Year.from(LocalDate.parse(rawMovie.getReleaseDate()));
            List<MovieInfo.Character> cast = rawCast.stream()
                    .map(personCast -> new MovieInfo.Character(personCast.getCharacter(), personCast.getName()))
                    .collect(Collectors.toList());
            return Optional.of(new MovieInfo(rawMovie.getTitle(), releaseYear, cast));
        } catch (IndexOutOfBoundsException e) {
            return Optional.empty();
        }
    }
}
